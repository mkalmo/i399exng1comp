var gulp = require('gulp');
var inject = require('gulp-inject');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var angularFilesort = require('gulp-angular-filesort');
var del = require('del');

var paths = {
    code: './app/**/*.js',
    codeMin: 'app.min.js',
    statics: ['app/**/*.html', 'img/**/*.*', '*.css'],
    dist: './dist',
    index: './index.html'
};

gulp.task('default', ['copy-static'], function () {

    var appScripts = gulp.src(paths.code)
        .pipe(angularFilesort())
        .pipe(concat(paths.codeMin))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist));

    return gulp
        .src(paths.index)
        .pipe(gulp.dest(paths.dist))
        .pipe(inject(appScripts, { relative: true }))
        .pipe(gulp.dest(paths.dist));

});

gulp.task('copy-static', ['clean'], function() {
    return gulp.src(paths.statics, { base: './' })
        .pipe(gulp.dest(paths.dist));
});

gulp.task('clean', function() {
    return new Promise(function(resolve, reject) {
        del(paths.dist, function() {
            resolve();
        });
    });
});
