(function () {
    'use strict';

    var options = {
        templateUrl: 'app/switch.cmp.html',
        controller: Controller,
        bindings: {}
    };

    function Controller() {
        this.$onInit = $onInit;

        function $onInit() {
        }
    }

    angular.module('app').component('switch', options);

})();

