(function () {
    'use strict';

    angular.module('app').controller('BoardCtrl', Ctrl);

    function Ctrl() {
        var vm = this;
        vm.toggleAlarm = toggleAlarm;

        vm.state = { light: false, alarm: false };

        function toggleAlarm() {
            vm.state.alarm = !vm.state.alarm;
        }
    }

})();


